import { Injectable } from '@angular/core';
import { Recipe } from '../../models/recipe.models';
import { RecipeResponse } from '../../models/recipe-response.models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  obs: any;
  private readonly GET_RECIPE: string = 'obter-receita';

  constructor( private httpClient: HttpClient) { }

  getRecipeByParameters(recipeParam: Recipe) {
    return this.httpClient.post<RecipeResponse>(environment.urlBackend + this.GET_RECIPE, JSON.stringify(recipeParam), {
      headers: { 'Content-Type': 'application/json', }
    });
  }
}
