import { Injectable } from '@angular/core';
import { Storage } from '../../models/storage.models';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  get<T>(chave: string): T {
    if (chave != null) {
      const itemStorage = JSON.parse(localStorage.getItem(chave)) as Storage<T>;

      if (itemStorage != null && itemStorage.expiration == null) {
        return itemStorage.object;
      }

      if (itemStorage == null || new Date(itemStorage.expiration) <= new Date()) {
        localStorage.setItem(chave, null);
        return null;
      }

      return itemStorage.object;
    }

    return null;
  }

  save(chave: string, objeto: any, expiracao: Date): void {
    const itemStorage = new Storage();

    itemStorage.object = objeto;
    itemStorage.expiration = expiracao;

    localStorage.setItem(chave, JSON.stringify(itemStorage));
  }
}
