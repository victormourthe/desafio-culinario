export class RecipeResponse {
    constructor() { }

    nome: string;
    link: string;
    preparo: string;
    rendimento: string;
}
