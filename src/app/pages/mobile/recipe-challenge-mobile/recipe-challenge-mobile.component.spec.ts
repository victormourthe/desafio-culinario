import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeMobileComponent } from './recipe-challenge-mobile.component';

describe('RecipeChallengeMobileComponent', () => {
  let component: RecipeChallengeMobileComponent;
  let fixture: ComponentFixture<RecipeChallengeMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
