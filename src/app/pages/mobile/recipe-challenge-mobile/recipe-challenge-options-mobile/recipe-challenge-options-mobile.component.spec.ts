import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeOptionsMobileComponent } from './recipe-challenge-options-mobile.component';

describe('RecipeChallengeOptionsMobileComponent', () => {
  let component: RecipeChallengeOptionsMobileComponent;
  let fixture: ComponentFixture<RecipeChallengeOptionsMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeOptionsMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeOptionsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
