import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { RecipeChallengeOptionsMobileComponent } from './recipe-challenge-options-mobile/recipe-challenge-options-mobile.component';

@Component({
  selector: 'app-recipe-challenge-mobile',
  templateUrl: './recipe-challenge-mobile.component.html',
  styleUrls: ['./recipe-challenge-mobile.component.scss']
})
export class RecipeChallengeMobileComponent implements OnInit {

  constructor(private bottomSheet: MatBottomSheet) { }

  ngOnInit() {
  }

  openBottomSheet(): void {
    this.bottomSheet.open(RecipeChallengeOptionsMobileComponent, {
      panelClass: 'custom-bottom-sheet',
    });
  }
}
