import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeResultMobileComponent } from './recipe-challenge-result-mobile.component';

describe('RecipeChallengeResultMobileComponent', () => {
  let component: RecipeChallengeResultMobileComponent;
  let fixture: ComponentFixture<RecipeChallengeResultMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeResultMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeResultMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
