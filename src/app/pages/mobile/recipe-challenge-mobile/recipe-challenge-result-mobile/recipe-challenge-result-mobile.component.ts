import { Component, OnInit } from '@angular/core';
import { RecipeResponse } from 'src/app/shared/models/recipe-response.models';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from 'src/app/shared/services/google-analytics/google-analytics.service';
import { MatBottomSheet } from '@angular/material';
import { RecipeChallengeOptionsMobileComponent } from '../recipe-challenge-options-mobile/recipe-challenge-options-mobile.component';

@Component({
  selector: 'app-recipe-challenge-result-mobile',
  templateUrl: './recipe-challenge-result-mobile.component.html',
  styleUrls: ['./recipe-challenge-result-mobile.component.scss']
})
export class RecipeChallengeResultMobileComponent implements OnInit {
  recipe: RecipeResponse;

  constructor(private bottomSheet: MatBottomSheet, private storageService: StorageService, private router: Router, public googleAnalyticsService: GoogleAnalyticsService) { }

  ngOnInit() {
    const recipeInStorage = this.storageService.get<RecipeResponse>('recipe');

    if (recipeInStorage != null) {
      this.recipe = this.storageService.get('recipe');
    }
    else {
      this.router.navigateByUrl('')
    }
  }

  anotherRecipe() {
    this.bottomSheet.open(RecipeChallengeOptionsMobileComponent, {
      panelClass: 'custom-bottom-sheet',
    });
    this.router.navigateByUrl('')
    this.googleAnalyticsService.eventEmitter("Escolheu outra Receita", "usuario", "clique");
  }

  navigateToRecipeSite() {
    window.open(this.recipe.link, "_blank");
    this.googleAnalyticsService.eventEmitter("Acessou site da Receita", "usuario", "clique");
  }

}
