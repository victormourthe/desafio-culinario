import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-challenge',
  templateUrl: './recipe-challenge.component.html',
  styleUrls: ['./recipe-challenge.component.scss']
})
export class RecipeChallengeComponent implements OnInit {
  home: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  openOptions() {
    this.home = !this.home;
  }

}
