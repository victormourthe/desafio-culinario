import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeOptionsComponent } from './recipe-challenge-options.component';

describe('RecipeChallengeOptionsComponent', () => {
  let component: RecipeChallengeOptionsComponent;
  let fixture: ComponentFixture<RecipeChallengeOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
