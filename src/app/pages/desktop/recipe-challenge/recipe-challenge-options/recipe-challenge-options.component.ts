import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/shared/services/recipe/recipe.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { Router } from '@angular/router';
import { Recipe } from 'src/app/shared/models/recipe.models';

@Component({
  selector: 'app-recipe-challenge-options',
  templateUrl: './recipe-challenge-options.component.html',
  styleUrls: ['./recipe-challenge-options.component.scss']
})
export class RecipeChallengeOptionsComponent implements OnInit {
  showLoading = false;
  veggie = 0;
  person = 0;
  level = "";

  formCompleted = false;

  constructor(private recipeService: RecipeService, private storage: StorageService, private router: Router) { }

  ngOnInit() {
    this.detectFormIsCompleted();
  }

  detectFormIsCompleted() {
    if (this.person === 0 || this.level === "") {
      this.formCompleted = false;

      document.getElementById('submit').style.setProperty('--bg-color', "#efefef");
      document.getElementById('submit').style.setProperty('--shadow', "none");
    }
    else {
      this.formCompleted = true;

      document.getElementById('submit').style.setProperty('--bg-color', "linear-gradient(to right, #f9c64f, #f9a935)");
      document.getElementById('submit').style.setProperty('--shadow', "-13px -18px 30px -20px #f9c64f");
    }
  }

  setVeggie() {
    if (this.veggie === 0) {
      this.veggie = 1
    }
    else {
      this.veggie = 0
    }
  }

  selectPerson(numberOfPerson: number) {
    this.person = numberOfPerson;

    let bgColor = "#efefef";
    let bgColorDefault = "#ffffff";

    if (numberOfPerson === 1) {
      document.getElementById('person-one').style.setProperty('--bg-color', bgColor);
      document.getElementById('person-two').style.setProperty('--bg-color', bgColorDefault);
      document.getElementById('person-tree').style.setProperty('--bg-color', bgColorDefault);
    }
    else if (numberOfPerson === 2) {
      document.getElementById('person-one').style.setProperty('--bg-color', bgColorDefault);
      document.getElementById('person-two').style.setProperty('--bg-color', bgColor);
      document.getElementById('person-tree').style.setProperty('--bg-color', bgColorDefault);
    }
    else {
      document.getElementById('person-one').style.setProperty('--bg-color', bgColorDefault);
      document.getElementById('person-two').style.setProperty('--bg-color', bgColorDefault);
      document.getElementById('person-tree').style.setProperty('--bg-color', bgColor);
    }

    this.detectFormIsCompleted();
  }

  selectLevel(levelSelected: string) {
    this.level = levelSelected;

    let bgColor = "#efefef";
    let bgColorDefault = "#ffffff";

    if (levelSelected === "rookie") {
      document.getElementById('level-rookie').style.setProperty('--bg-color', bgColor);
      document.getElementById('level-master').style.setProperty('--bg-color', bgColorDefault);
    }
    else {
      document.getElementById('level-rookie').style.setProperty('--bg-color', bgColorDefault);
      document.getElementById('level-master').style.setProperty('--bg-color', bgColor);
    }

    this.detectFormIsCompleted();
  }

  submitChallenge() {
    if (!this.showLoading && this.formCompleted) {
      var recipeFinal = new Recipe();

      recipeFinal.tipo = this.veggie;
      recipeFinal.preparo = this.level;
      recipeFinal.rendimento = this.person;

      this.showLoading = true;

      this.recipeService.getRecipeByParameters(recipeFinal).subscribe((response) => {
        const expiration = new Date();

        expiration.setDate(expiration.getDate() + 1);

        this.storage.save('recipe', response, expiration)
        this.showLoading = false;
        this.router.navigateByUrl('/resultado/desktop');
      });
    }
  }

  refreshPage(){
    window.location.reload();
  }

}
