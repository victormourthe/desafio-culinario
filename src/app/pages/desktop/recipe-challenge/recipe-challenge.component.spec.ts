import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeComponent } from './recipe-challenge.component';

describe('RecipeChallengeComponent', () => {
  let component: RecipeChallengeComponent;
  let fixture: ComponentFixture<RecipeChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
