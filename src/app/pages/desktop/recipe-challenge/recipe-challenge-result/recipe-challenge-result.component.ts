import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { RecipeResponse } from 'src/app/shared/models/recipe-response.models';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from 'src/app/shared/services/google-analytics/google-analytics.service';
declare let ga: Function;

@Component({
  selector: 'app-recipe-challenge-result',
  templateUrl: './recipe-challenge-result.component.html',
  styleUrls: ['./recipe-challenge-result.component.scss']
})
export class RecipeChallengeResultComponent implements OnInit {
  recipe: RecipeResponse;

  constructor(private storageService: StorageService, private router: Router, public googleAnalyticsService: GoogleAnalyticsService) { }

  ngOnInit() {
    const recipeInStorage = this.storageService.get<RecipeResponse>('recipe');

    if (recipeInStorage != null) {
      this.recipe = this.storageService.get('recipe');
    }
    else {
      this.router.navigateByUrl('')
    }

  }

  anotherRecipe() {
    this.router.navigateByUrl('')
    this.googleAnalyticsService.eventEmitter("Escolheu outra Receita", "usuario", "clique");
  }

  navigateToRecipeSite() {
    window.open(this.recipe.link, "_blank");
    this.googleAnalyticsService.eventEmitter("Acessou site da Receita", "usuario", "clique");
  }

}
