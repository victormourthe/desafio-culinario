import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeChallengeResultComponent } from './recipe-challenge-result.component';

describe('RecipeChallengeResultComponent', () => {
  let component: RecipeChallengeResultComponent;
  let fixture: ComponentFixture<RecipeChallengeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeChallengeResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeChallengeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
