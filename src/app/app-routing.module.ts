import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeChallengeResultComponent } from './pages/desktop/recipe-challenge/recipe-challenge-result/recipe-challenge-result.component';
import { HomeComponent } from './pages/home/home.component';
import { RecipeChallengeResultMobileComponent } from './pages/mobile/recipe-challenge-mobile/recipe-challenge-result-mobile/recipe-challenge-result-mobile.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'resultado',
    children: [
      {
        path: 'mobile',
        component: RecipeChallengeResultMobileComponent
      },
      {
        path: 'desktop',
        component: RecipeChallengeResultComponent
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
