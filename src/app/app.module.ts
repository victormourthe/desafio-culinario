import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecipeChallengeComponent } from './pages/desktop/recipe-challenge/recipe-challenge.component';
import { LoadingComponent } from './shared/components/loading/loading.component';
import { HttpClientModule } from '@angular/common/http';
import { RecipeChallengeResultComponent } from './pages/desktop/recipe-challenge/recipe-challenge-result/recipe-challenge-result.component';
import { RecipeService } from './shared/services/recipe/recipe.service';
import { GoogleAnalyticsService } from './shared/services/google-analytics/google-analytics.service';
import { HomeComponent } from './pages/home/home.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { RecipeChallengeMobileComponent } from './pages/mobile/recipe-challenge-mobile/recipe-challenge-mobile.component';
import { RecipeChallengeResultMobileComponent } from './pages/mobile/recipe-challenge-mobile/recipe-challenge-result-mobile/recipe-challenge-result-mobile.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RecipeChallengeOptionsMobileComponent } from './pages/mobile/recipe-challenge-mobile/recipe-challenge-options-mobile/recipe-challenge-options-mobile.component';
import { RecipeChallengeOptionsComponent } from './pages/desktop/recipe-challenge/recipe-challenge-options/recipe-challenge-options.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    RecipeChallengeComponent,
    LoadingComponent,
    RecipeChallengeResultComponent,
    HomeComponent,
    RecipeChallengeMobileComponent,
    RecipeChallengeResultMobileComponent,
    RecipeChallengeOptionsMobileComponent,
    RecipeChallengeOptionsComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatBottomSheetModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [GoogleAnalyticsService, RecipeService, DeviceDetectorService],
  bootstrap: [AppComponent],
  entryComponents: [RecipeChallengeOptionsMobileComponent]
})
export class AppModule { }
